<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="javascript:void(0)">
                <img src="/img/brand/blue.png" class="navbar-brand-img" alt="...">
            </a>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="/">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">داشبورد</span>
                        </a>
                    </li>
                    @if(Auth::user()->roles->role_id == '1')
                    <li class="nav-item">
                        <a class="nav-link" href="/users">
                            <i class="ni ni-planet text-orange"></i>
                            <span class="nav-link-text">مدیریت کاربران</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/category">
                            <i class="ni ni-planet text-orange"></i>
                            <span class="nav-link-text">گروه بندی مقالات</span>
                        </a>
                    </li>
                    @endif

                    <li class="nav-item">
                        <a class="nav-link" href="/articles">
                            <i class="ni ni-planet text-orange"></i>
                            <span class="nav-link-text">مدیریت مقالات</span>
                        </a>
                    </li>
                </ul>
                <!-- Navigation -->
            </div>
        </div>
    </div>
</nav>