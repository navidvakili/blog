@extends('layouts.master')

@section('pagetitle')کاربر جدید@endsection
@section('content')

@if($errors->any())
<div class="alert alert-danger">{{$errors->first()}}</div>
@endif
<form method="post" action="/users">
    @csrf
    <div class="form-group row">
        <label for="username" class="col-sm-2 col-form-label">نام کاربری</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="username" name="username" value="{{old('username')}}" autocomplete="off">
        </div>
    </div>
    <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label">گذرواژه</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="password" name="password"" autocomplete=" off">
        </div>
    </div>
    <div class="form-group row">
        <label for="password_confirmation" class="col-sm-2 col-form-label">تکرار گذرواژه</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" autocomplete="off">
        </div>
    </div>
    <div class="form-group row">
        <label for="fname" class="col-sm-2 col-form-label">نام</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="fname" name="fname" value="{{old('fname')}}" autocomplete="off">
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-2 col-form-label">نام خانوادگی</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="lname" name="lname" value="{{old('lname')}}" autocomplete="off">
        </div>
    </div>
    <div class="form-group row">
        <label for="mobile" class="col-sm-2 col-form-label">شماره همراه</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="mobile" name="mobile" value="{{old('mobile')}}" autocomplete="off">
        </div>
    </div>

    <div class="form-group row">
        <label for="mobile" class="col-sm-2 col-form-label">نقش کاربر</label>
        <div class="col-sm-10">
            @foreach ($roles as $k=>$role)
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="role" id="role{{$k}}" value="{{$role->id}}" @if($k==0) checked @endif>
                <label class="form-check-label" for="role{{$k}}">{{$role->title}}</label>
            </div>
            @endforeach
        </div>
    </div>

    <button type="submit" class="btn btn-primary">ثبت</button>
</form>
@endsection