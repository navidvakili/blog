@extends('layouts.master')

@section('content')
<a href="/users/create" class="btn btn-primary">جدید</a>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">نام</th>
      <th scope="col">نام خانوادگی</th>
      <th scope="col">وضعیت</th>
    </tr>
  </thead>
  <tbody>
    @php($key=0)
    @foreach($users as $user)
    <tr>
      <td>{{$users->firstItem() + $key}}</td>
      <td>{{$user->fname}}</td>
      <td>{{$user->lname}}</td>
      <td>
        <a href="/users/{{$user->username}}/edit">ویرایش</a> |
        <a href="/users/{{$user->username}}" onclick="event.preventDefault(); document.getElementById('form-delete-{{$user->username}}').submit();">حذف</a>
        <form action="/users/{{$user->username}}" id="form-delete-{{$user->username}}" method="post">
          @method('DELETE')
          @csrf</form>
      </td>
    </tr>
    @php($key++)
    @endforeach
  </tbody>
</table>
<div class="d-flex justify-content-center">{!! $users->links( "pagination::bootstrap-4") !!}</div>
@endsection