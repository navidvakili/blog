@extends('layouts.master')

@section('pagetitle')ویرایش مقاله@endsection
@section('content')

@if($errors->any())
<div class="alert alert-danger">{{$errors->first()}}</div>
@endif
<form method="post" action="/articles">
    @csrf
    <div class="form-group row">
        <label for="title" class="col-sm-2 col-form-label">عنوان</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}" autocomplete="off">
        </div>
    </div>
    <div class="form-group row">
        <label for="category" class="col-sm-2 col-form-label">گروه مقاله</label>
        <div class="col-sm-10">
            <select class="form-control" id="category" name="category">
                <option value=""></option>
                @foreach ($categories as $cat)
                <option value="{{$cat->id}}">{{$cat->title}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="summary" class="col-sm-2 col-form-label">خلاصه</label>
        <div class="col-sm-10">
            <textarea class="form-control" id="summary" name="summary">{{old('summary')}}</textarea>
        </div>
    </div>
    <div class="form-group row">
        <label for="text" class="col-sm-2 col-form-label">متن کامل مقاله</label>
        <div class="col-sm-10">
            <textarea class="form-control" id="text" name="text" rows="10">{{old('text')}}</textarea>
        </div>
    </div>

    <div class="form-group row">
        <label for="active" class="col-sm-2 col-form-label">وضعیت</label>
        <div class="col-sm-10">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="active" id="active1" value="1" checked>
                <label class="form-check-label" for="active1">فعال</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="active" id="active0" value="0">
                <label class="form-check-label" for="active0">غیرفعال</label>
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-primary">ثبت</button>
</form>
@endsection