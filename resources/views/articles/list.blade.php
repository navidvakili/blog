@extends('layouts.master')

@section('content')
<a href="/articles/create" class="btn btn-primary">مقاله جدید</a>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">نام مقاله</th>
      <th scope="col">نام نویسنده</th>
      <th scope="col">تاریخ انتشار</th>
      <th scope="col">وضعیت</th>
    </tr>
  </thead>
  <tbody>
    @php($key=0)
    @foreach($articles as $article)
    <tr>
      <td>{{$articles->firstItem() + $key}}</td>
      <td>{{$article->title}}</td>
      <td>{{$article->user->fname}} {{$article->user->lname}}</td>
      <td>{{(new Verta($article->created_at))->format('Y-n-j H:i')}}</td>
      <td>
        <a href="/articles/{{$article->id}}/edit">ویرایش</a> |
        <a href="/articles/{{$article->id}}" onclick="event.preventDefault(); if(confirm('آیا از حذف مطمئن هستید؟'))document.getElementById('form-delete-{{$article->id}}').submit();">حذف</a>
        <form action="/articles/{{$article->id}}" id="form-delete-{{$article->id}}" method="post">
          @method('DELETE')
          @csrf</form>
      </td>
    </tr>
    @php($key++)
    @endforeach
  </tbody>
</table>
<div class="d-flex justify-content-center">{!! $articles->links( "pagination::bootstrap-4") !!}</div>
@endsection