<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ورود به سامانه مدیریت</title>
    <link rel="stylesheet" href="{{mix('/css/styles.css')}}">
</head>

<body>
    <div class="container">
        <div class="col-xs-1" align="center">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">ورود به سامانه</h3>
                        </div>
                        <div class="panel-body">
                            <form accept-charset="UTF-8" role="form" action="{{route('login')}}" method="post">
                                @csrf
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control @error('username') is-invalid @enderror" placeholder="نام کاربری" name="username" type="text">
                                        @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control  @error('password') is-invalid @enderror" placeholder="گذرواژه" name="password" type="password">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" type="submit" value="ورود">
                                </fieldset>
                            </form>
                            <hr />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>