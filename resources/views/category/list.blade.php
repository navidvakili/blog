@extends('layouts.master')

@section('content')
<a href="/category/create" class="btn btn-primary">گروه جدید</a>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">نام گروه</th>
      <th scope="col">وضعیت</th>
    </tr>
  </thead>
  <tbody>
    @php($key=0)
    @foreach($categories as $cat)
    <tr>
      <td>{{$categories->firstItem() + $key}}</td>
      <td>{{$cat->title}}</td>
      <td>
        <a href="/category/{{$cat->id}}/edit">ویرایش</a> |
        <a href="/category/{{$cat->id}}" onclick="event.preventDefault(); if(confirm('آیا از حذف مطمئن هستید؟'))document.getElementById('form-delete-{{$cat->id}}').submit();">حذف</a>
        <form action="/category/{{$cat->id}}" id="form-delete-{{$cat->id}}" method="post">
          @method('DELETE')
          @csrf</form>
      </td>
    </tr>
    @php($key++)
    @endforeach
  </tbody>
</table>
<div class="d-flex justify-content-center">{!! $categories->links( "pagination::bootstrap-4") !!}</div>
@endsection