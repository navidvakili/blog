@extends('layouts.master')

@section('pagetitle')گروه جدید@endsection
@section('content')

@if($errors->any())
<div class="alert alert-danger">{{$errors->first()}}</div>
@endif
<form method="post" action="/category">
    @csrf
    <div class="form-group row">
        <label for="title" class="col-sm-2 col-form-label">نام گروه</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}" autocomplete="off">
        </div>
    </div>

    <div class="form-group row">
        <label for="active" class="col-sm-2 col-form-label">وضعیت</label>
        <div class="col-sm-10">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="active" id="active1" value="1" checked>
                <label class="form-check-label" for="active1">فعال</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="active" id="active0" value="0">
                <label class="form-check-label" for="active0">غیرفعال</label>
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-primary">ثبت</button>
</form>
@endsection