const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles(
    ["resources/css/bootstrap.min.css", "resources/css/style.css"],
    "public/css/styles.css"
).version();

mix.scripts(
    ["resources/js/bootstrap.bundle.min.js", "resources/js/main.js"],
    "public/js/scripts.js"
).version();

mix.styles(
    [
        "resources/vendor/@fortawesome/fontawesome-free/css/all.min.css",
        "resources/css/nucleo.css",
        "resources/css/argon.css",
    ],
    "public/css/admin.css"
).version();

mix.scripts(
    [
        "resources/vendor/jquery/dist/jquery.min.js",
        "resources/vendor/bootstrap/dist/js/bootstrap.bundle.min.js",
        "resources/vendor/js-cookie/js.cookie.js",
        "resources/vendor/jquery.scrollbar/jquery.scrollbar.min.js",
        "resources/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js",
        "resources/vendor/chart.js/dist/Chart.min.js",
        "resources/vendor/chart.js/dist/Chart.extension.js",
        "resources/js/argon.js",
    ],
    "public/js/admin.js"
).version();
