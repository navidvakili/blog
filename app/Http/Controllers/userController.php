<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $access = ['admin'];
        if (!in_array(Auth::user()->role, $access))
            abort(403, 'شما دسترسی به این صفحه ندارید');
        $users = User::where('username', '<>', Auth::user()->username)->orderBy('created_at', 'desc')->paginate(20);
        return view('users.list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $access = ['admin'];
        if (!in_array(Auth::user()->role, $access))
            abort(403, 'شما دسترسی به این صفحه ندارید');
        $roles = Role::all();
        return view('users.new', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => [
                'required',
                'string',
                'min:6',             // must be at least 10 characters in length
                'required_with:password_confirmation',
                'same:password_confirmation',
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
            ],
            'password_confirmation' => 'min:6',
            'fname' => 'required',
            'lname' => 'required',
            'mobile' => 'required',
            'role' => 'required',
        ]);

        $user = User::find($request->username);
        if ($user != null)
            return redirect()->back()->withErrors('نام کاربری انتخاب شده تکراری است');

        $user = new User();
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->mobile = $request->mobile;
        $user->save();

        $role = new UserRole();
        $role->role_id = $request->role;
        $role->username = $request->username;
        $role->save();

        return redirect()->to('/users')->with('message', 'کاربر با موفقیت ذخیره شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $access = ['admin', 'writer'];
        if (!in_array(Auth::user()->role, $access))
            abort(403, 'شما دسترسی به این صفحه ندارید');
        $roles = Role::all();
        return view('users.edit', compact('roles', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'mobile' => 'required',
            'role' => 'required',
        ]);


        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->mobile = $request->mobile;
        $user->save();

        $user1 = UserRole::where('username', $user->username)->first();
        if ($user1 != null) $user1->delete();
        $role = new UserRole();
        $role->role_id = $request->role;
        $role->username = $user->username;
        $role->save();

        return redirect()->to('/users')->with('message', 'کاربر با موفقیت ویرایش شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $articles = Article::where('username', $user->username)->count();
        if ($articles > 0) {
            return redirect()->back()->withErrors('این کاربر بدلیل داشتن مقاله امکان حذف وجود ندارد');
        }

        $user->delete();

        return redirect()->to('users')->with('message', 'کاربر ' . $user->username . ' با موفقیت حذف شد');
    }
}
