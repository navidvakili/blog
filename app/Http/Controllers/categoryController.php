<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('created_at', 'desc')->paginate(10);
        return view('category.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'active' => 'required',
        ]);

        $cat = new Category();
        $cat->title = $request->title;
        $cat->active = $request->active;
        $cat->save();

        return redirect()->to('category')->with('message', 'گروه ' . $request->title . ' با موفقیت ذخیره شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'title' => 'required',
            'active' => 'required',
        ]);

        $category->title = $request->title;
        $category->active = $request->active;
        $category->save();

        return redirect()->to('category')->with('message', 'گروه ' . $request->title . ' با موفقیت ویرایش شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $articles = Article::where('category_id', $category->id)->count();
        if ($articles > 0) {
            return redirect()->back()->withErrors('این گروه بدلیل داشتن مقاله امکان حذف وجود ندارد');
        }

        $category->delete();

        return redirect()->to('category')->with('message', 'گروه ' . $category->title . ' با موفقیت حذف شد');
    }
}
